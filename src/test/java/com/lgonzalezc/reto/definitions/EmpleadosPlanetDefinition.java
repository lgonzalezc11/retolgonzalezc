package com.lgonzalezc.reto.definitions;

import com.lgonzalezc.reto.steps.EmpleadosPlanetSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class EmpleadosPlanetDefinition {
	
@Steps
	EmpleadosPlanetSteps empleadosplanetsteps;
	
@Given("^Soy un empleado registrado$")
	public void soy_un_empleado_registrado() throws Throwable {
	   empleadosplanetsteps.Abrirurlplanet();
}


@When("^Ingreso usuario \"([^\"]*)\"$")
public void ingresoUsuario(String strUsuario) throws Throwable {
   	empleadosplanetsteps.ingresoUsuario(strUsuario);  
}

@When("^ingreso contraseña \"([^\"]*)\"$")
public void ingreso_contraseña(String contraseña) throws Throwable {
    empleadosplanetsteps.ingresocontraseña(contraseña);   
}

@Then("^Verifico el ingreso a plataforma$")
public void verifico_el_ingreso_a_plataforma() throws Throwable {
    empleadosplanetsteps.verificarhome();
}

@Then("^ingreso al directorio$")
public void ingresardirectorio() throws Throwable {
    empleadosplanetsteps.ingresardirectorio();
}

@Then("^ingreso a empleados$")
public void ingresaraempleados() throws Throwable {
    empleadosplanetsteps.ingresaraempleados();
}

@Then("^Ingreso filtronombre \"([^\"]*)\"$")
public void filtronombre(String nombre) throws Throwable {
    empleadosplanetsteps.filtronombre(nombre);  
}

@Then("^Ingreso filtroapellido \"([^\"]*)\"$")
public void filtroapellido(String apellido) throws Throwable {
   empleadosplanetsteps.filtroapellido(apellido);
}

@Then("^Verifico la busqueda$")
public void verificarbusqueda() throws Throwable {
   empleadosplanetsteps.verificarbusqueda();
}

@Then("^acceder al empleado$")
public void accederempleado() throws Throwable {
    empleadosplanetsteps.accederempleado();
}

@Then("^extraer datos del empleado$")
public void extracciondatos() throws Throwable {
    empleadosplanetsteps.extracciondatos();
}

}