package com.lgonzalezc.reto.steps;

import com.lgonzalezc.reto.pageobjects.EmpleadosPlanetPageObjects;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class EmpleadosPlanetSteps {

	EmpleadosPlanetPageObjects Empleadosplanetpageobjects;
	@Step
	public void Abrirurlplanet() {		
		Empleadosplanetpageobjects.open();
		Serenity.takeScreenshot();	
	}
	
    @Step
	public void ingresoUsuario(String strUsuario) {
    	Empleadosplanetpageobjects.ingresousuario(strUsuario);
    	Serenity.takeScreenshot();
	}
    
    @Step
	public void ingresocontraseña(String contraseña) {
    	Empleadosplanetpageobjects.ingresocontraseña(contraseña);
    	Serenity.takeScreenshot();
	}
    
	public void verificarhome() {
		Empleadosplanetpageobjects.verificarhome();
		Serenity.takeScreenshot();	
	}
	
	@Step
	public void ingresardirectorio() {
		Empleadosplanetpageobjects.ingresardirectorio();
		Serenity.takeScreenshot();	
	}
	
	@Step	
	public void ingresaraempleados() {
		Empleadosplanetpageobjects.ingresaraempleados();
		Serenity.takeScreenshot();	
	}
	
	@Step
	public void filtronombre(String nombre) {
		Empleadosplanetpageobjects.filtronombre(nombre);
	}
	
	@Step
	public void filtroapellido(String apellido) {
		Empleadosplanetpageobjects.filtroapellido(apellido);
	}
	
	@Step
	public void verificarbusqueda() {
		Empleadosplanetpageobjects.verificarbusqueda();
		Serenity.takeScreenshot();	
	}
	
	@Step
	public void accederempleado() {
		Empleadosplanetpageobjects.accederempleado();
		Serenity.takeScreenshot();	
	}
	
	@Step
	public void extracciondatos() {
		Empleadosplanetpageobjects.extracciondatos();
	Serenity.takeScreenshot();	
	}
}


