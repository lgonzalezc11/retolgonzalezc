package com.lgonzalezc.reto.pageobjects;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("http://planet.choucairtesting.com/bin/login/Main/WebHome?origurl=/")

public class EmpleadosPlanetPageObjects extends PageObject{

	public  void ingresousuario(String usuario) {
		find(By.name("username")).type(usuario); 
	}

	public  void ingresocontraseña(String contraseña) {	
		find(By.name("password")).type(contraseña); 
	}

	public void verificarhome() {	
		find(By.xpath("//input[@value='Iniciar']")).click();
	}

	public void ingresardirectorio() {		
		find(By.xpath("//*[@id=\"patternSideBarContents\"]/span[7]/a")).click();
	}
	
	public void ingresaraempleados() {		
		find(By.xpath("//*[@id=\"patternMainContents\"]/div[3]/div[1]/ul/li[1]/a")).click(); 
	}
	
	public void filtronombre(String nombre) {		
		find(By.name("nombre")).type(nombre); 
	}
	
	public void filtroapellido(String apellido) {		
		find(By.name("apellidos")).type(apellido); 
	}
	
	public void verificarbusqueda() {		
		find(By.xpath("//*[@id=\"patternMainContents\"]/div[3]/div[1]/div[1]/table/tbody/tr[4]/td/img")).click();
	}
	
	public void accederempleado() {		
		find(By.xpath("//*[@id=\"searchresult\"]/tr[1]/td[2]/a")).click();
	}
	
	public void extracciondatos() {
		for (String winHandle : getDriver().getWindowHandles()) {
			getDriver().switchTo().window(winHandle); 
			
		}
	}
}

	


