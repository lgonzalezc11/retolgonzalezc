<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en_US" lang="en_US"><head>
<title> WebHome &lt; Main &lt; Planet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="icon" href="http://planet.choucairtesting.com/pub/System/LogosChoucair/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://planet.choucairtesting.com/pub/System/LogosChoucair/favicon.ico" type="image/x-icon" />
<link rel="alternate" href="http://planet.choucairtesting.com/bin/edit/Main/WebHome?_T=2019/08/14" type="application/x-wiki" title="edit WebHome" />
<meta name="WEBTOPIC" content="WebHome" />
<meta name="WEB" content="Main" />
<meta name="SCRIPTURLPATH" content="/bin" />
<meta name="SCRIPTSUFFIX" content="" />
<meta name="TEXT_JUMP" content="Ir a" />
<meta name="TEXT_SEARCH" content="Buscar" />
<meta name="TEXT_NUM_TOPICS" content="Número de temas:" />
<meta name="TEXT_MODIFY_SEARCH" content="Modificar búsqueda" />
<meta name="robots" content="noindex" /><link rel="alternate" type="application/rss+xml" title="RSS Feed" href="/bin/view/Main/WebRss" />
<base href="http://planet.choucairtesting.com/bin/view/Main/WebHome" />

<!--BEHAVIOURCONTRIB--><script type="text/javascript" async="" src="http://www.google-analytics.com/ga.js"></script><script type="text/javascript" src="/pub/System/BehaviourContrib/behaviour.compressed.js"></script>
<script type="text/javascript" src="/pub/System/JavascriptFiles/foswikilib.js"></script>
<script type="text/javascript" src="/pub/System/JavascriptFiles/foswikiWindow.js"></script>
<script type="text/javascript" src="/pub/System/JavascriptFiles/foswikiEvent.js"></script>
<script type="text/javascript" src="/pub/System/JavascriptFiles/foswikiHTML.js"></script>
<script type="text/javascript" src="/pub/System/JavascriptFiles/foswikiCSS.js"></script>
<script type="text/javascript" src="/pub/System/JavascriptFiles/foswikiForm.js"></script>
<script type="text/javascript" src="/pub/System/PatternSkin/pattern.js"></script><style type="text/css" media="all">
@import url('/pub/System/SkinTemplates/base.css');
</style>
<style type="text/css" media="all">
@import url('/pub/System/PatternSkinChoucair/layout.css');
@import url('/pub/System/PatternSkinChoucair/style.css');
@import url('/pub/System/PatternSkinChoucair/colors.css');
</style>
<style type="text/css" media="all">
@import url('/pub/System/PatternSkinChoucair/column_left.css');
@import url('/pub/System/PatternSkinChoucair/theme_foswiki_noframe.css');
</style><style type="text/css" media="all">
	/* Styles that are set using variables */
	.patternBookView .foswikiTopRow,
	.patternWebIndicator a img,
	.patternWebIndicator a:hover img {
		background-color:#065168;
	}
	

.patternBookView {
	border-color:#065168;
}
.patternPreviewPage #patternMain {
	/* uncomment to set the preview image */
	/*background-image:url("/pub/System/PreviewBackground/preview2bg.gif");*/
}
</style>
<style type="text/css" media="all">
	@import url("/pub/System/PatternSkin/print.css");
</style>
<!--[if IE]><style type="text/css" media="screen">
pre {
	height:1%;
	overflow-x:auto;
}
</style>
<![endif]-->

<script type="text/javascript" src="/pub/System/JavascriptFiles/foswikiStyles.js"></script><style type="text/css" media="all">.foswikiMakeVisible{display:inline;}.foswikiMakeVisibleInline{display:inline;}.foswikiMakeVisibleBlock{display:block;}.foswikiMakeHidden{display:none;}</style>
<link rel="stylesheet" href="/pub/System/JQueryPlugin/plugins/tabpane/jquery.tabpane.css?version=1.2.1" type="text/css" media="all" /> <!-- JQUERYPLUGIN::TABPANE -->
<link rel="stylesheet" href="/pub/System/EditChapterPlugin/ecpstyles.css" type="text/css" media="all" /> <!-- EDITCHAPTERPLUGIN -->
<meta name="foswiki.PUBURL" content="http://planet.choucairtesting.com/pub" /> <!-- PUBURL -->
<meta name="foswiki.PUBURLPATH" content="/pub" /> <!-- PUBURLPATH -->
<meta name="foswiki.SCRIPTSUFFIX" content="" /> <!-- SCRIPTSUFFIX -->
<meta name="foswiki.SCRIPTURL" content="http://planet.choucairtesting.com/bin" /> <!-- SCRIPTURL -->
<meta name="foswiki.SCRIPTURLPATH" content="/bin" /> <!-- SCRIPTURLPATH -->
<meta name="foswiki.SERVERTIME" content="2019/08/14%20-%2002:45" /> <!-- SERVERTIME -->
<meta name="foswiki.SKIN" content="choucair%2cpattern" /> <!-- SKIN -->
<meta name="foswiki.SYSTEMWEB" content="System" /> <!-- SYSTEMWEB -->
<meta name="foswiki.TOPIC" content="WebHome" /> <!-- TOPIC -->
<meta name="foswiki.USERNAME" content="lgonzalezc" /> <!-- USERNAME -->
<meta name="foswiki.USERSWEB" content="Main" /> <!-- USERSWEB -->
<meta name="foswiki.WEB" content="Main" /> <!-- WEB -->
<meta name="foswiki.WIKINAME" content="LeidyVivianaGonzalezCanaria" /> <!-- WIKINAME -->
<meta name="foswiki.WIKIUSERNAME" content="Main.LeidyVivianaGonzalezCanaria" /> <!-- WIKIUSERNAME -->
<meta name="foswiki.NAMEFILTER" content="%5b%5cs%5c*%3f~%5e%5c%24%40%25%22'%26%3b%7c%3c%3e%5c%5b%5c%5d%5cx00-%5cx1f%5d" /> <!-- NAMEFILTER -->
<meta name="foswiki.TWISTYANIMATIONSPEED" content="%25TWISTYANIMATIONSPEED%25" /> <!-- TWISTYANIMATIONSPEED --> <!-- JQUERYPLUGIN::FOSWIKI::META -->
<script type="text/javascript" src="/pub/System/JQueryPlugin/jquery-1.3.2.js"></script> <!-- JQUERYPLUGIN -->
<script type="text/javascript" src="/pub/System/JQueryPlugin/plugins/livequery/jquery.livequery.js?version=1.1.1"></script> <!-- JQUERYPLUGIN::LIVEQUERY -->
<script type="text/javascript" src="/pub/System/JQueryPlugin/plugins/foswiki/jquery.foswiki.js?version=2.01"></script> <!-- JQUERYPLUGIN::FOSWIKI -->
<script type="text/javascript" src="/pub/System/JQueryPlugin/plugins/metadata/jquery.metadata.js?version=2.1ef2bb44c86f5d0e98d55"></script> <!-- JQUERYPLUGIN::METADATA -->
<script type="text/javascript" src="/pub/System/JQueryPlugin/plugins/easing/jquery.easing.js?version=1.3"></script> <!-- JQUERYPLUGIN::EASING -->
<script type="text/javascript" src="/pub/System/JQueryPlugin/plugins/tabpane/jquery.tabpane.js?version=1.2.1"></script>
<script type="text/javascript" src="/pub/System/JQueryPlugin/plugins/tabpane/jquery.tabpane.init.js?version=1.2.1"></script> <!-- JQUERYPLUGIN::TABPANE -->
<script type="text/javascript" src="/pub/System/JQueryPlugin/plugins/bgiframe/jquery.bgiframe.js?version=2.1.2"></script> <!-- JQUERYPLUGIN::BGIFRAME -->
 <!-- 584 -->
</head><body class="patternViewPage foswikiJs">
<a name="PageTop"></a><div class="foswikiPage"><div id="patternScreen">
<div id="patternPageShadow">
<div id="patternPage">
<div id="patternWrapper"><div id="patternOuter">
<div id="patternFloatWrap">
<div id="patternMain"><div id="patternClearHeaderCenter"></div>
<div id="patternMainContents"><div class="patternTop"><span class="patternHomePath foswikiLeft"><span class="patternHomePathTitle">Usted está aquí: </span><a href="/bin/view/Main/WebHome" title="lfnanclares - 2018/05/08 - 15:00 - r1.335: " class="foswikiCurrentTopicLinkfoswikiCurrentWebHomeLink">Planet</a><span class="foswikiSeparator">&gt;</span><a href="/bin/view/Main/WebHome">Web Main</a><span class="foswikiSeparator">&gt;</span><a href="http://planet.choucairtesting.com/bin/view/Main/WebHome" title="Revisión del tema: 335 (2018/05/08 - 15:00:00)">WebHome</a> <span class="patternRevInfo">(2018/05/08, <a href="/bin/view/Main/LuisaFernandaNanclaresVelez" title="RegistrationAgent - 2017/04/01 - 09:00 - r1.1: Set ALLOWTOPICCHANGE GestionDeUsuariosGroup FirstName: Luisa Fernanda LastName: Nanclares Vélez">LuisaFernandaNanclaresVelez</a>)</span></span><!-- /patternHomePath--><span class="patternToolBar foswikiRight"><span class="foswikiRequiresChangePermission"><a href="http://planet.choucairtesting.com/bin/edit/Main/WebHome?t=1565768740" rel="nofollow" title="Editar texto de este tema" accesskey="e"><span class="foswikiAccessKey">E</span>ditar</a></span><span class="foswikiRequiresChangePermission"><a href="/bin/attach/Main/WebHome" rel="nofollow" title="Adjuntar una imagen o documento a este tema" acceskey="a"><span class="foswikiAccessKey">A</span>djuntar</a></span></span><!-- /patternToolBar--><br class="foswikiClear" /></div><!--/patternTop--><div class="foswikiContentHeader"></div><div class="patternContent"><div class="foswikiTopic">
<style>
#wrap {}
#col1 {width: 80%; float: left; border-right: solid 1px #e2e2e2;}
#col2 {width: 0%; float: left; border-right: solid 1px #e2e2e2;}
#col3 {width: 19%; float: left;}
.box {margin-left: 10px; margin-right: 10px; margin-bottom: 20px; border-bottom: solid 1px #e2e2e2; padding-bottom: 0px;}
.results {min-height: 40px;}
.item {margin: 5px; padding: 5px;}
.item img {float: left; margin-right: 10px; width: 50px;}
.item .results img {width: auto;}
.more {float: right; margin-top: 20px;}
.more a {text-decoration: none;}
ul.jqTabGroup li.current {background-color: #CCC;}
.clearDiv {clear: both;height: 0;}
.eventdate {color: #999; font-size: 80%;}
.linkSaludo :link:focus, :link, :link:active {
background-color:transparent;
color: rgb(0,0,106);
}
.texto {
	 padding-top: 30px;
}
.linkSaludo A:visited {
	 color: rgb(83,79,68) important;
}
</style>
<div id="saludo" style="">
<h1><a name="Bienvenid_LeidyVivianaGonzalezCa"></a> Bienvenid@ <a href="/bin/view/Main/LeidyVivianaGonzalezCanaria" title="RegistrationAgent - 2017/10/17 - 13:24 - r1.1: Set ALLOWTOPICCHANGE GestionDeUsuariosGroup FirstName: Leidy Viviana LastName: Gonzalez Canaria">LeidyVivianaGonzalezCanaria</a> a Planet, su cargo <a class="linkSaludo" href="http://planet.choucairtesting.com/bin/view/Registros.Cargos.AnalistaDePruebas">Analista de Pruebas</a> y su cliente es <a href="/bin/view/Main/LeidyVivianaGonzalezCanaria" title="RegistrationAgent - 2017/10/17 - 13:24 - r1.1: Set ALLOWTOPICCHANGE GestionDeUsuariosGroup FirstName: Leidy Viviana LastName: Gonzalez Canaria">No Cliente asignado</a> </h1>
<b><marquee width="820" bgcolor="gray" scrolldelay="175"><font color="White"> </font><i><font color="lime"> </font></i><font color="White"> </font></marquee></b>
</div>
<hr />
<div id="wrap">
<div class="col" id="col1">
	<div class="box" id="empresa">
<div class="jqTabPane jqTabPaneDefault {select:'1', autoMaxExpand:false, animate:false, minHeight:230} jqInitedTabpane jqTabPaneInitialized"><ul class="jqTabGroup"><li class="current"><a href="#" data="jqTab1991">Choucair</a></li></ul><span class="foswikiClear"></span>
<!-- TAB --><div id="jqTab1991" class=" jqTab current">

<div class="jqTabContents">
<div class="texto" style="padding-top: 0px;"><b>Bienvenidos a Planet</b>, un espacio dedicado al sistema de gestión de la compañía, acá encontraras los procesos de la compañía, lineamientos y documentos de interés general.
<p>
</p><center><img alt="&#9;Medios_de_comunicacion_Choucair_V2.png" src="http://planet.choucairtesting.com/pub/Main/WebHome/Medios_de_comunicacion_Choucair_V2.png" title="Medios_de_comunicacion_Choucair_V2.png" /></center>
</div>
</div></div><!-- //ENDTAB -->
</div><!-- //ENDTABPANE -->
	 </div>
</div>
<div class="col" id="col3">
	<div class="box" id="sociales" style="margin: 0 0 0 5px;padding: 0 0 0 5px;">
<h1><a name="Sociales_de_Bogota"></a> Sociales de Bogota </h1>
	  <div class="item" id="cumpleanos" style="margin: 0;padding: 0;">
	  <table>
	<tbody><tr>
			<td valign="center" align="left">
				<img alt="Cumpleaños" title="Cumpleaños" src="http://planet.choucairtesting.com/pub/Main/WebHome/icon-cumpleanos.png" style="width: 30px;margin: 0;" />
	 </td>
		 <td valign="top" align="left">
	   <div class="results">  <div class="loaderCumpleanos" style="padding: 10px; display: none;"><img width="16" alt="processing" align="top" src="/pub/System/DocumentGraphics/processing.gif" height="16" border="0" /></div>
			  <span class="itemtext" style="font-size: 90%;color: #999;">Proximos Cumpleaños: </span><br /><span class="itemtext" style="font-size: 85%;"><a href="/Main.DianaShirleyMoralesTejada">Diana Shirley Morales Tejada</a><span class="fechaCumpleanos" style="color: #999;"> (8/14)</span></span>,<br /><span class="itemtext" style="font-size: 85%;"><a href="/Main.EdwinOmarRodriguezCruz">Edwin Omar Rodriguez Cruz</a><span class="fechaCumpleanos" style="color: #999;"> (8/22)</span></span>,<br /><span class="itemtext" style="font-size: 85%;"><a href="/Main.JhonFredyRendonQuintero">Jhon Fredy Rendon Quintero</a><span class="fechaCumpleanos" style="color: #999;"> (8/20)</span></span>,<br /><span class="itemtext" style="font-size: 85%;"><a href="/Main.JuanGuillermoJaramilloSerna">Juan Guillermo Jaramillo Serna</a><span class="fechaCumpleanos" style="color: #999;"> (8/14)</span></span>,<br /><span class="itemtext" style="font-size: 85%;"><a href="/Main.JulianDavidSalasGranados">Julian David Salas Granados</a><span class="fechaCumpleanos" style="color: #999;"> (8/15)</span></span>,<br /><span class="itemtext" style="font-size: 85%;"><a href="/Main.MauricioAndresVargasAlvarez">Mauricio Andres Vargas Alvarez</a><span class="fechaCumpleanos" style="color: #999;"> (8/18)</span></span></div>
	 </td>
		 </tr>
	  </tbody></table>
	 </div>
	 <div class="item" id="matrimonios" style="margin: 0;padding: 0;">
	  <table>
	<tbody><tr>
			<td valign="center" align="left">
		<img alt="Matrimonios" title="Matrimonios" src="http://planet.choucairtesting.com/pub/Main/WebHome/icon-matrimonio.png" style="width: 30px;margin: 0;" />
	 </td>
		 <td valign="top" align="left">
		<div class="results">  <div class="loader" style="padding: 10px; display: none;"><img width="16" alt="processing" align="top" src="/pub/System/DocumentGraphics/processing.gif" height="16" border="0" /></div>
		<span class="itemtext" style="font-size: 90%;color: #999;">Matrimonios del mes: </span><br /></div>
	 </td>
		 </tr>
		</tbody></table>
	 </div>
	 <div class="item" id="nacimientos" style="margin: 0;padding: 0;">
	  <table>
	<tbody><tr>
			<td valign="center" align="left">
		<img alt="Nacimientos" title="Nacimientos" src="http://planet.choucairtesting.com/pub/Main/WebHome/icon-nacimiento.png" style="width: 30px;margin: 0;" />
	 </td>
		 <td valign="top" align="left">
		<div class="results">  <div class="loader" style="padding: 10px; display: none;"><img width="16" alt="processing" align="top" src="/pub/System/DocumentGraphics/processing.gif" height="16" border="0" /></div><span class="itemtext" style="font-size: 90%;color: #999;">Nacimientos del mes: </span><br /></div>
	 </td>
		 </tr>
		</tbody></table>
	 </div>
	 <div class="more">
		<a href="http://planet.choucairtesting.com/bin/view/Empleados/Eventos?tipos=Registros.TiposCategorias.TipoId20101006093605;ciudad=Bogota;pais=Colombia;" id="masSociales">VER MÁS...</a>
	 </div>
	 <div style="clear:both;"> </div>
	</div>
</div>
<div style="clear: both;"> </div>
</div>
<div id="test"></div>
<p>
</p><p>
<script type="text/javascript">
foswiki.tiposEventosCorporativos = 'Registros.TiposCategorias.TipoId20101006100956';
foswiki.tiposEventosSociales = 'Registros.TiposCategorias.TipoId20101006093605';
foswiki.tiposNoticias = 'Registros.TiposCategorias.TipoId20110119123717';
foswiki.fechaNow = new Date();
foswiki.fechaFinal = new Date();
foswiki.fechaDelta = foswiki.fechaFinal.setDate(foswiki.fechaNow.getDate() + 8);
foswiki.fechaFinal.setTime(foswiki.fechaDelta); 
foswiki.ciudad = 'Bogota';
foswiki.pais = 'Colombia';
if (foswiki.ciudad == 'xxx'){
	 foswiki.ciudadclean = "(.*)";
}else{
	var cleanStringregExp = RegExp('[^a-zA-z0-9/\._]','igm');
	foswiki.ciudadclean = foswiki.ciudad.replace(cleanStringregExp, '.');
}
if (foswiki.pais == 'xxx'){
	 foswiki.paisclean = "(.*)";
}else{
	var cleanStringregExp = RegExp('[^a-zA-z0-9/\._]','igm');
	foswiki.paisclean = foswiki.pais.replace(cleanStringregExp, '.');
}
foswiki.masEventos = "http://planet.choucairtesting.com/bin/view/Empleados/Eventos?tipos=Registros.TiposCategorias.TipoId20101006100956;ciudad=" + foswiki.ciudadclean + ";pais=" + foswiki.paisclean + ";";
foswiki.masSociales = "http://planet.choucairtesting.com/bin/view/Empleados/Eventos?tipos=Registros.TiposCategorias.TipoId20101006093605;ciudad=" + foswiki.ciudadclean + ";pais=" + foswiki.paisclean + ";";
foswiki.masNoticas = "http://planet.choucairtesting.com/bin/view/Empleados/Noticias?tipos=Registros.TiposCategorias.TipoId20110119123717;promocionadas=all;";
foswiki.otrasCiudadesEventos = "http://planet.choucairtesting.com/bin/view/Empleados/Eventos?tipos=Registros.TiposCategorias.TipoId20101006100956;ciudad=(.*);pais=(.*);vertodos=1";
$(function (){
	$('#masEventos').attr('href', foswiki.masEventos);
	$('#otrasCiudadesEventos').attr('href', foswiki.otrasCiudadesEventos);
	$('#masSociales').attr('href', foswiki.masSociales);
	$('#masNoticias').attr('href', foswiki.masNoticas);
});
var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i&lt;data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();
$(function (){
	var regexp = new RegExp('Explorer[67]','img');
	var broserdetect = BrowserDetect.browser + BrowserDetect.version;
	if (broserdetect.search(regexp) &lt; 0){
		  $('.texto').css('padding-top', '0px');
	}
});
</script>
<script type="text/javascript" src="http://planet.choucairtesting.com/pub/Estructuras/ScriptsMainWebHome/MainWebHomeNews.js"></script>
<script type="text/javascript" src="http://planet.choucairtesting.com/pub/Estructuras/ScriptsMainWebHome/MainWebHomeEvents.js"></script>
<script type="text/javascript" src="http://planet.choucairtesting.com/pub/Estructuras/ScriptsMainWebHome/MainWebHomeSociales.js"></script>
</p><p>
</p></div><!-- /foswikiTopic-->
<div class="foswikiContentFooter"></div></div><!-- /patternContent-->
<a name="topic-actions"></a><div class="patternTopicActions"><div class="patternTopicAction"><span class="patternActionButtons"><span class="foswikiRequiresChangePermission"><a href="http://planet.choucairtesting.com/bin/edit/Main/WebHome?t=1565768740" rel="nofollow" title="Editar texto de este tema" accesskey="e"><span class="foswikiAccessKey">E</span>ditar</a></span><span class="foswikiSeparator"> | </span><span class="foswikiRequiresChangePermission"><a href="/bin/attach/Main/WebHome" rel="nofollow" title="Adjuntar una imagen o documento a este tema" acceskey="a"><span class="foswikiAccessKey">A</span>djuntar</a></span><span class="foswikiSeparator"> | </span><span><a href="/bin/view/Main/WebHome?cover=print;validation_key=d545f0533a575715e7091a8dd860965f" rel="nofollow" title="Vista de impresión de este tema" accesskey="p">Vista de im<span class="foswikiAccessKey">p</span>resión</a></span><span class="foswikiSeparator"> | </span><span><a href="/bin/genpdf/Main/WebHome?validation_key=d545f0533a575715e7091a8dd860965f" rel="nofollow" title="PDF version of this topic" accesskey="p"><span class="foswikiAccessKey">P</span>DF</a></span><span class="foswikiSeparator"> | </span><span><span class="foswikiRequiresChangePermission"><a href="/bin/oops/Main/WebHome?template=oopshistory" rel="nofollow" title="View historial completo del tema" accesskey="h"><span class="foswikiAccessKey">H</span>istorial</a></span>: r335 <a rel="nofollow" href="/bin/rdiff/Main/WebHome?rev1=335;rev2=334">&lt;</a> <a rel="nofollow" href="/bin/view/Main/WebHome?rev=334">r334</a> <a rel="nofollow" href="/bin/rdiff/Main/WebHome?rev1=334;rev2=333">&lt;</a> <a rel="nofollow" href="/bin/view/Main/WebHome?rev=333">r333</a> <a rel="nofollow" href="/bin/rdiff/Main/WebHome?rev1=333;rev2=332">&lt;</a> <a rel="nofollow" href="/bin/view/Main/WebHome?rev=332">r332</a> <a rel="nofollow" href="/bin/rdiff/Main/WebHome?rev1=332;rev2=331">&lt;</a> <a rel="nofollow" href="/bin/view/Main/WebHome?rev=331">r331</a></span><span class="foswikiSeparator"> | </span><span><a href="/bin/oops/Main/WebHome?template=backlinksweb" rel="nofollow" title="Buscar la web Main por temas que estén enlazados aquí" accesskey="b">Enlaces entrantes (<span class="foswikiAccessKey">b</span>)</a></span><span class="foswikiSeparator"> | </span><span><a href="/bin/view/Main/WebHome?raw=on" rel="nofollow" title="View código sin formato" accesskey="r">Ve<span class="foswikiAccessKey">r</span> código</a></span><span class="foswikiSeparator"> | </span><span class="foswikiRequiresChangePermission"><a href="http://planet.choucairtesting.com/bin/edit/Main/WebHome?t=1565768740;nowysiwyg=1" rel="nofollow" title="Editar código de este tema" accesskey="w">Editar código (<span class="foswikiAccessKey">w</span>)</a></span><span class="foswikiSeparator"> | </span><span><a href="/bin/oops/Main/WebHome?template=oopsmore&amp;param1=335&amp;param2=335" rel="nofollow" title="Borrar o renombrar este tema; establecer tema padre; ver y comparar revisiones" accesskey="m"><span class="foswikiAccessKey">M</span>ás acciones de tema</a></span><span class="foswikiSeparator"> | </span><span class="foswikiSeparator"> | </span><span class="foswikiSeparator"> | </span><span><a href="/bin/view/Main/WebHome?validation_key=d545f0533a575715e7091a8dd860965f#patternMain" rel="nofollow" title="Volver arriba"><span class="foswikiAccessKey">V</span>olver arriba</a></span></span></div><!--/patternTopicAction--></div><!--/patternTopicActions--><div class="patternInfo foswikiGrayText"><div class="patternRevInfo">Revisión del tema: r335 - 2018/05/08 - 15:00:00 - <a href="/bin/view/Main/LuisaFernandaNanclaresVelez" title="RegistrationAgent - 2017/04/01 - 09:00 - r1.1: Set ALLOWTOPICCHANGE GestionDeUsuariosGroup FirstName: Luisa Fernanda LastName: Nanclares Vélez">LuisaFernandaNanclaresVelez</a></div><!-- /patternRevInfo--><div class="patternMoved"></div><!-- /patternMoved--></div><!-- /patternInfo-->
</div><!-- /patternMainContents-->
</div><!-- /patternMain--><div id="patternSideBar"><div id="patternClearHeaderLeft"></div>
<div id="patternSideBarContents">
<p>
<style>
.menuLevel1 {font-weight: bold;}
</style>
</p><center><a href="/bin/view/Main/WebHome" title="lfnanclares - 2018/05/08 - 15:00 - r1.335: " class="foswikiCurrentTopicLinkfoswikiCurrentWebHomeLink"><img border="0" src="/pub/Estructuras/WebLeftBarCairo/cairologo.jpg" title="Enlarge" alt="Enlarge" /></a></center>
<hr />
<form action="http://planet.choucairtesting.com/bin/view/Main/WebSearch" method="GET">
<input type="submit" value="Buscar»" />
</form>
<hr />
<div class="patternLeftBarPersonal">
Bienvenido<br /> <a href="/bin/view/Main/LeidyVivianaGonzalezCanaria" title="RegistrationAgent - 2017/10/17 - 13:24 - r1.1: Set ALLOWTOPICCHANGE GestionDeUsuariosGroup FirstName: Leidy Viviana LastName: Gonzalez Canaria">Leidy Viviana Gonzalez Canaria</a><br /> <ul><li class="patternLogOut"><a href="http://planet.choucairtesting.com/bin/view/Main/WebHome?logout=1">Logout</a></li></ul>
</div>
<hr />
<span style="white-space:nowrap;" class="menuLevel1"><a style="text-decoration:block;" href="/bin/view/Main/WebHome"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/blue-dot.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Home</a></span><br />
<span style="white-space:nowrap;margin:0; padding: 0;" class="menuLevel1"><a style="text-decoration:none;" href="javascript: toggleAreas();"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-dot.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Info Área</a></span><br />
<div id="sub_areas" style="display: block;margin: 0;padding:0;">
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="/bin/view/Metodologia/ManualDelSistemaDeGestionP0"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Sistema de Gestión</a></span><br />
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="/bin/view/TalentoHumano/WebHomeSaludOcupacional"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> SST</a></span><br />
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="/bin/view/PremiosReconocimientosYCertificaciones/WebHome"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Premios, reconocimientos <br />      y certificaciones</a></span><br />
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="/bin/view/GestionAdministrativa/WebHome"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Gestion Administrativa</a></span><br />
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="/bin/view/DocumentosDeInteres/"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Documentos Corporativos</a></span><br />
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="/bin/view/Metodologia/PlantillasGenerales/WebHome"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Plantillas Corporativas</a></span><br />
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="/bin/view/InformacionGerencial/TiposInformes/WebHome"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Informacion Gerencial</a></span><br />
<p>
</p><p>
</p></div>
<span style="white-space:nowrap;margin:0; padding: 0;" class="menuLevel1"><a style="text-decoration:none;" href="javascript: toggleMetodos();"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-dot.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Metodología</a></span><br />
<div id="sub_metodos" style="display: block;margin: 0;padding:0;">
<span style="white-space:nowrap;margin: 0 0 0 10px;" class="menuLevel2"><a style="text-decoration:none;" href="/bin/view/Metodologia/WebHome"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Procesos Choucair</a></span><br />
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="http://planet.choucairtesting.com/pub/Metodologia/PrestacionDelServicio/Testing/PruebasEnUnaPagina/pruebasenunapagina.html" target="”_blank”"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Pruebas en una Página</a></span><br />
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="/bin/view/Metodologia/DocumentacionPublica/ToolboxCH"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> ToolBox</a></span><br />
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="/bin/view/Empleados/BibliotecaChoucair"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Biblioteca</a></span><br />
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="/bin/view//Metodologia/TerminologiaComercial/WebHome"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Terminología Comercial</a></span><br />
</div>
<span style="white-space:nowrap;" class="menuLevel1"><a style="text-decoration:none;" href="javascript: toggleOperaciones();"><img src="http://planet.choucairtesting.com/pub/Main/WebLeftBarCairo/black-dot.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Operaciones</a></span><br />
<div id="sub_operaciones" style="display: none;">
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2"><a style="text-decoration:none;" href="http://planet.choucairtesting.com/Metodologia/PrestacionDelServicio/Testing/MaxTime/WebHome" target="_blank"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-line.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Max Time</a></span><br />
<span style="white-space:nowrap;margin-left: 10px;" class="menuLevel2">
</span></div>
<span style="white-space:nowrap;" class="menuLevel1"><a style="text-decoration:none;" href="/bin/view/Empleados/CafeChoucair"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-dot.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Café Choucair</a></span><br />
<span style="white-space:nowrap;" class="menuLevel1"><a style="text-decoration:none;" href="/bin/view/Empleados/EspirituChoucair"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-dot.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Espíritu Choucair</a></span><br />
<span style="white-space:nowrap;" class="menuLevel1"><a style="text-decoration:none;" href="/bin/view/Empleados/Directorio"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-dot.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Encuéntrame</a></span><br /> 
<span style="white-space:nowrap;" class="menuLevel1"><a style="text-decoration:none;" href="/bin/view/WikiTips/WebHome"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-dot.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> WikiTips<img src="/pub/Estructuras/WebLeftBarCairo/star.png" alt="star.png" width="12" style="margin-bottom: 3px;" /></a></span><br />
<p>
</p><p>
<span style="white-space:nowrap;" class="menuLevel1"><a style="text-decoration:none;" href="/bin/view/Main/LeidyVivianaGonzalezCanaria"><img src="http://planet.choucairtesting.com/pub/Estructuras/WebLeftBarCairo/black-dot.png" border="0" alt="" width="5" height="5" style="vertical-align: baseline;" /> Mi Perfil</a></span><br />
</p><p>
</p><p>
</p><p>
</p><p>
</p><p>
</p><p>
</p><p>
<script type="text/javascript">
function toggleOperaciones (){
   if ($('#sub_operaciones').css('display') == "none"){
       $('#sub_operaciones').css('display', 'block');
   }else{
       $('#sub_operaciones').css('display', 'none');
   }
}
function toggleMetodos (){
   if ($('#sub_metodos').css('display') == "none"){
       $('#sub_metodos').css('display', 'block');
       $('#metodos_bluedot').css('display','inline');
       $('#metodos_blackdot').css('display', 'none');
   }else{
       $('#sub_metodos').css('display', 'none');
       $('#metodos_blackdot').css('display', 'inline');
       $('#metodos_bluedot').css('display','none');
   }
}
function toggleAreas (){
   if ($('#sub_areas').css('display') == "none"){
       $('#sub_areas').css('display', 'block');
       $('#areas_bluedot').css('display','inline');
       $('#areas_blackdot').css('display', 'none');
   }else{
       $('#sub_areas').css('display', 'none');
       $('#areas_blackdot').css('display', 'inline');
       $('#areas_bluedot').css('display','none');
   }
}
$(function (){
   if((foswiki.getPreference('TOPIC') == 'BusquedaDeToolbox' &amp;&amp; foswiki.getPreference('WEB') == 'Empleados') || (foswiki.getPreference('WEB') == 'Metodologia') || (foswiki.getPreference('WEB') == 'Portafolio') || (foswiki.getPreference('TOPIC') == 'BibliotecaChoucair' &amp;&amp; foswiki.getPreference('WEB') == 'Empleados') || (foswiki.getPreference('WEB') == 'Metodologia/EspecificidadServiciosYProductos') || foswiki.getPreference('WEB').search('Toolbox') &gt; 0){
      &lt;!--toggleMetodos(); --&gt;
  }
   if( foswiki.getPreference('TOPIC').search('Directorio') &lt; 0 &amp;&amp; foswiki.getPreference('WEB').search('Directorio') &lt; 0 &amp;&amp; ((foswiki.getPreference('WEB') == 'Main' &amp;&amp; foswiki.getPreference('TOPIC') == 'WebHome')  || (foswiki.getPreference('WEB') == 'Empleados' &amp;&amp; foswiki.getPreference('TOPIC') != 'CafeChoucair')|| foswiki.getPreference('WEB') == 'Mercadeo' || foswiki.getPreference('WEB') == 'TalentoHumano' || foswiki.web == 'GestionDelConocimiento') || foswiki.getPreference('WEB').search('Eventos') &gt; 0 || foswiki.getPreference('WEB').search('Noticias') &gt; 0 || foswiki.getPreference('WEB').search('Recomendados') &gt; 0 || foswiki.getPreference('WEB').search('kaizen') &gt; 0){
      toggleChoucair();
  }
});
function actionSearch(){
   $('#quickSearchBox').focus();
   return 1;
}
</script>
</p><p>
</p><p>
</p><hr />
<p>
</p></div><!-- /patternSideBarContents--></div><!-- /patternSideBar-->
</div><!-- /patternFloatWrap-->
<div class="clear"> </div>
</div><!-- /patternOuter--></div><!-- /patternWrapper--><div id="patternTopBar"><div id="patternTopBarContents"><style>
.patternContent {background: #FFFFFF url(/pub/Main/SitePreferences/qs.png) no-repeat 100% 100%;}
#patternTopBar {position: fixed;}
.patternMetaMenu ul li {display: block; margin-top: 5px;}
</style>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tbody><tr><td valign="top"><span id="foswikiLogo" class="foswikiImage"><a href="http://planet.choucairtesting.com/bin/view/Main/WebHome"><img src="http://planet.choucairtesting.com/pub/System/LogosChoucair/logochoucair.jpg" border="0" alt="Powered by Foswiki, The Free and Open Source Wiki" style="border:none;" /></a></span></td>
<td align="right" valign="bottom">
<h2 style="color: #FFFFFF; margin-right: 30px; font-size: 2.2em;">COCOA</h2>
</td>
<td width="400px" align="right" valign="top" class="patternMetaMenu">
 <ul>
<li> <form name="jumpForm" action="/bin/view/Main/WebHome"><input id="jumpFormField" type="text" class="foswikiInputField foswikiInputFieldBeforeFocus" name="topic" value="" size="18" /><noscript>&amp;nbsp;&lt;input type="submit" class="foswikiButton" size="5" name="submit" value="Ir a" /&gt;&amp;nbsp;</noscript>   </form>
</li> <li> <form name="quickSearchForm" action="/bin/view/Main/WebSearch"><input type="text" class="foswikiInputField foswikiInputFieldBeforeFocus" id="quickSearchBox" name="search" value="" size="18" /><input type="hidden" name="scope" value="all" /><input type="hidden" name="web" value="Main" /><noscript>&amp;nbsp;&lt;input type="submit" size="5" class="foswikiButton" name="submit" value="Buscar" /&gt;&amp;nbsp;</noscript>   </form>
</li> <li> <a style="color: white;" href="/bin/view/Main/WebHome?validation_key=d545f0533a575715e7091a8dd860965f#patternMain" rel="nofollow" title="Volver Arriba">Volver Arriba</a>
</li></ul> 
<!--   * <form name="changeLanguage" action="/bin/oops/Main/WebHome" method="get"><select name="language" class="foswikiSelect" onchange="document.changeLanguage.submit()"><option  value="en">English</option> <option selected="selected" value="es">Español</option></select><input type="hidden" name="template" value="oopslanguagechanged" /><noscript><input class="foswikiButton" type="submit" value="Cambiar idioma" /></noscript></form> -->
</td></tr></tbody></table></div></div><!-- /patternTopBar--><div id="patternBottomBar"><div id="patternBottomBarContents"><div id="patternWebBottomBar"><b>Choucair Cardenas Testing. Todos los derechos reservados.  © 2012 Choucair.</b><br />La información contenida en esta página, así como la información que contienen sus archivos adjuntos es información privilegiada y confidencial y pertenece de forma exclusiva a CHOUCAIR CARDENAS TESTING S.A., por lo que revelarla o copiarla a terceras personas le está prohibido. Queda notificado que la utilización, divulgación y/o copia del mismo está prohibida según las normas legales aplicables, pudiendo ser sujeto de sanciones, incluidas las penales.</div><!--/patternWebBottomBar--></div><!-- /patternBottomBarContents--></div><!-- /patternBottomBar-->
</div><!-- /patternPage-->
</div><!-- /patternPageShadow-->
</div><!-- /patternScreen-->
</div><!-- /foswikiPage--><!-- GOOGLEANALYTICSPLUGIN --><script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', "UA-23719701-1"]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>



</body></html>